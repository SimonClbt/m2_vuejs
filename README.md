# projet

> A Vue.js project
- Projet Vuejs de M2 :
- Réalisé :
- Les 5 pages demandées
- Generation et listing des listes
- Gestion du routage basique et nommé
- Intéractions avec les items de listes
- Gestion du localStorage
- Problème restant :
- lors de l'ajout d'item à une liste, il est possible d'ajouter un élément vide malgré une fonction devant filtrer ce genre de cas

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build
```

For detailed explanation on how things work, consult the [docs for vue-loader](http://vuejs.github.io/vue-loader).
