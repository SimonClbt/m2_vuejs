import router from './routes'
import Vue from 'vue'
import App from './App'

import 'bootstrap/dist/css/bootstrap.css'

new Vue({
  router,
  render: h => h(App),
}).$mount('#app')
