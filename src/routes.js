import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from './components/Home.vue'
import Listing from './components/Listing.vue'
import New from './components/New.vue'
import Liste from './components/Liste.vue'
import AddItem from './components/AddItem.vue'

Vue.use(VueRouter)

export default new VueRouter({
  routes: [
    {
      path : '/',
      name : 'home',
      component : Home
    }, {
      path : '/listing',
      name : 'listing',
      component : Listing
    }, {
      path : '/new',
      name : 'new',
      component : New
    }, {
      path : '/liste/:id',
      name : 'liste',
      component : Liste
    },,
    {
      path: '/liste/:id/add',
      name: 'addItem',
      component: AddItem,
    },
     {
      path : '*',
      redirect : '/'
    }
  ]
})
